from plugins.base import Plugin
from clint.textui import puts, colored
from plugins import TMP_FOLDER, SCRIPTS_FOLDER, VERSION
import tarfile
import sh
import os


class Tzdata(Plugin):
    url = "http://www.iana.org/time-zones/repository/releases/tzdata2017b.tar.gz"
    package = "tzdata"

    def unpack(self):

        puts(colored.cyan('Extracting'))
        tar = tarfile.open(self.filepath)
        tar.extractall(TMP_FOLDER)
        tar.close()
        sh.mkdir(os.path.join(TMP_FOLDER, self.package))

        for f in os.listdir(TMP_FOLDER):
            if os.path.isfile(os.path.join(TMP_FOLDER, f)) and ".gz" not in os.path.splitext(f)[1]:
                sh.mv(os.path.join(TMP_FOLDER, f), os.path.join(TMP_FOLDER, self.package))

        puts(colored.green('Complete!'))
