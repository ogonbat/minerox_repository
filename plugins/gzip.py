from plugins.base import Plugin


class Gzip(Plugin):
    url = "http://ftp.gnu.org/gnu/gzip/gzip-1.8.tar.xz"
    package = "gzip"
