from plugins.base import Plugin


class Xmlparser(Plugin):
    url = "http://cpan.metacpan.org/authors/id/T/TO/TODDR/XML-Parser-2.44.tar.gz"
    package = "xmlparser"
