from plugins.base import Plugin


class Intltool(Plugin):
    url = "http://launchpad.net/intltool/trunk/0.51.0/+download/intltool-0.51.0.tar.gz"
    package = "intltool"
