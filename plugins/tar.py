from plugins.base import Plugin


class Tar(Plugin):
    url = "http://ftp.gnu.org/gnu/tar/tar-1.29.tar.xz"
    package = "tar"
