from plugins.base import Plugin
from plugins import TMP_FOLDER
import os
import sh
import tarfile
from clint.textui import puts, colored

from plugins.udev import Udev


class Eudev(Plugin):
    url = "http://dev.gentoo.org/~blueness/eudev/eudev-3.2.2.tar.gz"
    package = "eudev"


    def unpack(self):
        puts(colored.cyan('Extracting'))
        tar = tarfile.open(self.filepath)
        tar.extractall(TMP_FOLDER)
        tar.close()

        self.tar_path = self.filename.replace(self.extension, '')
        if os.path.isdir(os.path.join(TMP_FOLDER, self.tar_path)):
            sh.mv('-v', os.path.join(TMP_FOLDER, self.tar_path), os.path.join(TMP_FOLDER, self.package))
        else:
            # maybe the folder is not the same as the name file
            diff_directory = [f for f in os.listdir(TMP_FOLDER) if os.path.isdir(os.path.join(TMP_FOLDER, f))]
            sh.mv('-v', os.path.join(TMP_FOLDER, diff_directory[0]), os.path.join(TMP_FOLDER, self.package))

        udev = Udev()
        udev.download()
        udev.unpack()
        # copy to the root folder
        sh.mv(os.path.join(TMP_FOLDER, "udev"), os.path.join(TMP_FOLDER, self.package))
        sh.rm('-rf', os.path.join(TMP_FOLDER, udev.filename))

        puts(colored.green('Complete!'))
