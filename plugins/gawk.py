from plugins.base import Plugin


class Gawk(Plugin):
    url = "http://ftp.gnu.org/gnu/gawk/gawk-4.1.4.tar.xz"
    package = "gawk"
