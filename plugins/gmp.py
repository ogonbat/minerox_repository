from plugins.base import Plugin


class Gmp(Plugin):
    url = "http://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.xz"
    package = "gmp"
