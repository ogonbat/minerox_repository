from plugins.base import Plugin


class Sed(Plugin):
    url = "http://ftp.gnu.org/gnu/sed/sed-4.4.tar.xz"
    package = "sed"
