from plugins.base import Plugin


class Inetutils(Plugin):
    url = "http://ftp.gnu.org/gnu/inetutils/inetutils-1.9.4.tar.xz"
    package = "inetutils"
