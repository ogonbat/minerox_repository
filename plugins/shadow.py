from plugins.base import Plugin


class Shadow(Plugin):
    url = "https://github.com/shadow-maint/shadow/releases/download/4.5/shadow-4.5.tar.xz"
    package = "shadow"
