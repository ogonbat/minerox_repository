from plugins.base import Plugin


class Linux(Plugin):
    url = "https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.12.7.tar.xz"
    package = "linux"
