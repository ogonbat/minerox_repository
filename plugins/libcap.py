from plugins.base import Plugin


class Libcap(Plugin):
    url = "https://www.kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-2.25.tar.xz"
    package = "libcap"
