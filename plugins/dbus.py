from plugins.base import Plugin


class Dbus(Plugin):
    url = "http://dbus.freedesktop.org/releases/dbus/dbus-1.10.22.tar.gz"
    package = "dbus"
