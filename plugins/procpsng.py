from plugins.base import Plugin


class Procpsng(Plugin):
    url = "http://sourceforge.net/projects/procps-ng/files/Production/procps-ng-3.3.12.tar.xz"
    package = "procpsng"
