from plugins.base import Plugin


class Binutils(Plugin):
    url = "http://ftp.gnu.org/gnu/binutils/binutils-2.29.tar.bz2"
    package = "binutils"
