from plugins.base import Plugin


class Xz(Plugin):
    url = "http://tukaani.org/xz/xz-5.2.3.tar.xz"
    package = "xz"
