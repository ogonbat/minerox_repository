from plugins.base import Plugin


class Readline(Plugin):
    url = "http://ftp.gnu.org/gnu/readline/readline-7.0.tar.gz"
    package = "readline"
