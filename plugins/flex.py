from plugins.base import Plugin


class Flex(Plugin):
    url = "https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz"
    package = "flex"
