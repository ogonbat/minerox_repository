from plugins.base import Plugin


class Bison(Plugin):
    url = "http://ftp.gnu.org/gnu/bison/bison-3.0.4.tar.xz"
    package = "bison"
