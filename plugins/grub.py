from plugins.base import Plugin


class Grub(Plugin):
    url = "http://ftp.gnu.org/gnu/grub/grub-2.02.tar.xz"
    package = "grub"
