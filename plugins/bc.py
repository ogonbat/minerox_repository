from plugins.base import Plugin


class Bc(Plugin):
    url = "http://ftp.gnu.org/gnu/bc/bc-1.07.1.tar.gz"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.0/bc-1.06.95-memory_leak-1.patch"
    package = "bc"
