from plugins.base import Plugin


class Gettext(Plugin):
    url = "http://ftp.gnu.org/gnu/gettext/gettext-0.19.8.1.tar.xz"
    package = "gettext"
