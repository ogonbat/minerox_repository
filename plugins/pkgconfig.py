from plugins.base import Plugin


class Pkgconfig(Plugin):
    url = "https://pkg-config.freedesktop.org/releases/pkg-config-0.29.2.tar.gz"
    package = "pkgconfig"
