from plugins.base import Plugin


class Perl(Plugin):
    url = "http://www.cpan.org/src/5.0/perl-5.26.0.tar.xz"
    package = "perl"
