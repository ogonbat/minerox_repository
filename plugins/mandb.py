from plugins.base import Plugin


class Mandb(Plugin):
    url = "http://download.savannah.gnu.org/releases/man-db/man-db-2.7.6.1.tar.xz"
    package = "mandb"
