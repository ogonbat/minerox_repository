import os

CURRENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
TMP_FOLDER = os.path.join(CURRENT_FOLDER, '..', 'tmp')
SCRIPTS_FOLDER = os.path.join(CURRENT_FOLDER, '..', 'scripts')
VERSION = '0.0.1'