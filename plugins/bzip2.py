from plugins.base import Plugin


class Bzip2(Plugin):
    url = "http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.0/bzip2-1.0.6-install_docs-1.patch"
    package = "bzip2"
