from plugins.base import Plugin


class Automake(Plugin):
    url = "http://ftp.gnu.org/gnu/automake/automake-1.15.1.tar.xz"
    package = "automake"
