from plugins.base import Plugin


class Zlib(Plugin):
    url = "http://zlib.net/zlib-1.2.11.tar.xz"
    package = "zlib"
