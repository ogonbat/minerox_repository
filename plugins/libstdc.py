from plugins.base import Plugin


class Libstdc(Plugin):
    url = "http://ftp.gnu.org/gnu/gcc/gcc-6.3.0/gcc-6.3.0.tar.bz2"
    package = "libstdc"
