from plugins.base import Plugin


class Grep(Plugin):
    url = "http://ftp.gnu.org/gnu/grep/grep-3.1.tar.xz"
    package = "grep"
