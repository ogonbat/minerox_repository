from plugins.base import Plugin
from plugins import TMP_FOLDER
import os
import sh
import tarfile
from clint.textui import puts, colored

from plugins.tzdata import Tzdata


class Glibc(Plugin):
    url = "http://ftp.gnu.org/gnu/glibc/glibc-2.26.tar.xz"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.1/glibc-2.26-fhs-1.patch"
    package = "glibc"

    def unpack(self):
        puts(colored.cyan('Extracting'))
        tar = tarfile.open(self.filepath)
        tar.extractall(TMP_FOLDER)
        tar.close()

        self.tar_path = self.filename.replace(self.extension, '')
        if os.path.isdir(os.path.join(TMP_FOLDER, self.tar_path)):
            sh.mv('-v', os.path.join(TMP_FOLDER, self.tar_path), os.path.join(TMP_FOLDER, self.package))
        else:
            # maybe the folder is not the same as the name file
            diff_directory = [f for f in os.listdir(TMP_FOLDER) if os.path.isdir(os.path.join(TMP_FOLDER, f))]
            sh.mv('-v', os.path.join(TMP_FOLDER, diff_directory[0]), os.path.join(TMP_FOLDER, self.package))

        tzdata = Tzdata()
        tzdata.download()
        tzdata.unpack()
        # copy to the root folder
        sh.mv(os.path.join(TMP_FOLDER, "tzdata", "glibc-2.26-fhs-1.patch"), TMP_FOLDER)
        sh.mv(os.path.join(TMP_FOLDER, "tzdata", "glibc-2.26.tar.xz"), TMP_FOLDER)
        sh.mv(os.path.join(TMP_FOLDER, "tzdata"), os.path.join(TMP_FOLDER, self.package))
        sh.rm('-rf', os.path.join(TMP_FOLDER, tzdata.filename))



        puts(colored.green('Complete!'))
