from plugins.base import Plugin


class Psmisc(Plugin):
    url = "https://sourceforge.net/projects/psmisc/files/psmisc/psmisc-23.1.tar.xz"
    package = "psmisc"
