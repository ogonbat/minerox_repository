from plugins.base import Plugin


class Coreutils(Plugin):
    url = "http://ftp.gnu.org/gnu/coreutils/coreutils-8.27.tar.xz"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.1/coreutils-8.27-i18n-1.patch"
    package = "coreutils"
