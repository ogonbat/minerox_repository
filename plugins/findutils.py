from plugins.base import Plugin


class Findutils(Plugin):
    url = "http://ftp.gnu.org/gnu/findutils/findutils-4.6.0.tar.gz"
    package = "findutils"
