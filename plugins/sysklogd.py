from plugins.base import Plugin


class Sysklogd(Plugin):
    url = "http://www.infodrom.org/projects/sysklogd/download/sysklogd-1.5.1.tar.gz"
    package = "sysklogd"
