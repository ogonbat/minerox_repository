from plugins.base import Plugin


class Sysvinit(Plugin):
    url = "http://download.savannah.gnu.org/releases/sysvinit/sysvinit-2.88dsf.tar.bz2"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.0/sysvinit-2.88dsf-consolidated-1.patch"
    package = "sysvinit"
