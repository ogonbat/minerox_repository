from plugins.base import Plugin


class Bootscripts(Plugin):
    url = "http://www.linuxfromscratch.org/lfs/downloads/8.1/lfs-bootscripts-20170626.tar.bz2"
    package = "bootscripts"
