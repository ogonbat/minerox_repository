from plugins.base import Plugin
from plugins import TMP_FOLDER
from plugins.gmp import Gmp
from plugins.mpc import Mpc
from plugins.mpfr import Mpfr
import os
import sh
import tarfile
from clint.textui import puts, colored



class Gcc(Plugin):
    url = "http://ftp.gnu.org/gnu/gcc/gcc-7.2.0/gcc-7.2.0.tar.xz"
    package = "gcc"

    def unpack(self):
        puts(colored.cyan('Extracting'))
        tar = tarfile.open(self.filepath)
        tar.extractall(TMP_FOLDER)
        tar.close()

        self.tar_path = self.filename.replace(self.extension, '')
        if os.path.isdir(os.path.join(TMP_FOLDER, self.tar_path)):
            sh.mv('-v', os.path.join(TMP_FOLDER, self.tar_path), os.path.join(TMP_FOLDER, self.package))
        else:
            # maybe the folder is not the same as the name file
            diff_directory = [f for f in os.listdir(TMP_FOLDER) if os.path.isdir(os.path.join(TMP_FOLDER, f))]
            sh.mv('-v', os.path.join(TMP_FOLDER, diff_directory[0]), os.path.join(TMP_FOLDER, self.package))

        mpfr = Mpfr()
        mpfr.download()
        mpfr.unpack()
        # copy to the root folder
        sh.mv(os.path.join(TMP_FOLDER, "mpfr"), os.path.join(TMP_FOLDER, self.package))
        sh.rm('-rf', os.path.join(TMP_FOLDER, mpfr.filename))

        gmp = Gmp()
        gmp.download()
        gmp.unpack()
        # copy to the root folder
        sh.mv(os.path.join(TMP_FOLDER, "gmp"), os.path.join(TMP_FOLDER, self.package))
        sh.rm('-rf', os.path.join(TMP_FOLDER, gmp.filename))

        mpc = Mpc()
        mpc.download()
        mpc.unpack()
        # copy to the root folder
        sh.mv(os.path.join(TMP_FOLDER, "mpc"), os.path.join(TMP_FOLDER, self.package))
        sh.rm('-rf', os.path.join(TMP_FOLDER, mpc.filename))

        puts(colored.green('Complete!'))
