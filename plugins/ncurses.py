from plugins.base import Plugin


class Ncurses(Plugin):
    url = "http://ftp.gnu.org/gnu//ncurses/ncurses-6.0.tar.gz"
    package = "ncurses"
