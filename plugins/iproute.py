from plugins.base import Plugin


class Iproute(Plugin):
    url = "https://www.kernel.org/pub/linux/utils/net/iproute2/iproute2-4.12.0.tar.xz"
    package = "iproute"
