from plugins.base import Plugin


class Systemd(Plugin):
    url = "http://anduin.linuxfromscratch.org/LFS/systemd-234-lfs.tar.xz"
    package = "systemd"
