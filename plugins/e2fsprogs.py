from plugins.base import Plugin


class E2fsprogs(Plugin):
    url = "http://downloads.sourceforge.net/project/e2fsprogs/e2fsprogs/v1.43.5/e2fsprogs-1.43.5.tar.gz"
    package = "e2fsprogs"
