from plugins.base import Plugin


class M4(Plugin):
    url = "http://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.xz"
    package = "m4"
