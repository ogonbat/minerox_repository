from plugins.base import Plugin


class Iana(Plugin):
    url = "http://anduin.linuxfromscratch.org/LFS/iana-etc-2.30.tar.bz2"
    package = "iana"
