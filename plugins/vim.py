from plugins.base import Plugin


class Vim(Plugin):
    url = "http://ftp.vim.org/ftp/ftp/vim/unix/vim-8.0.069.tar.bz2"
    package = "vim"
