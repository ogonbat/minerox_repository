import os
import requests
from clint.textui import puts, colored
from plugins import TMP_FOLDER, SCRIPTS_FOLDER, VERSION
import tarfile
import sh


class Plugin(object):
    url = None
    patch = None
    package = None
    filename = None
    extension = None
    filepath = None
    patchpath = None
    tar_path = None

    def check(self):
        if os.path.isfile(os.path.join("{}/{}.tar.gz".format(VERSION, self.package))):
            return True
        return False

    def download(self):
        # download the correspondent file
        self.filename = os.path.basename(self.url)
        self.filepath = os.path.join(TMP_FOLDER, self.filename)

        # get the real extension of the file
        if ".src." in self.filename:
            self.extension = ".src.tar{}".format(os.path.splitext(self.filename)[1])
        else:
            self.extension = ".tar{}".format(os.path.splitext(self.filename)[1])

        puts(colored.cyan('Download {}....'.format(self.package)))

        r = requests.get(self.url, stream=True, verify=False)
        with open(self.filepath, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
        puts(colored.green('Complete!'))

        if self.patch:
            puts(colored.cyan('Download Patch'))
            r = requests.get(self.patch, stream=True, verify=False)

            self.patchpath = os.path.join(TMP_FOLDER, os.path.basename(self.patch))
            with open(self.patchpath, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)

            puts(colored.green('Complete!'))

    def unpack(self):

        puts(colored.cyan('Extracting'))
        tar = tarfile.open(self.filepath)
        tar.extractall(TMP_FOLDER)
        tar.close()

        self.tar_path = self.filename.replace(self.extension, '')
        if os.path.isdir(os.path.join(TMP_FOLDER, self.tar_path)):
            sh.mv('-v', os.path.join(TMP_FOLDER, self.tar_path), os.path.join(TMP_FOLDER, self.package))
        else:
            # maybe the folder is not the same as the name file
            diff_directory = [f for f in os.listdir(TMP_FOLDER) if os.path.isdir(os.path.join(TMP_FOLDER, f))]
            sh.mv('-v', os.path.join(TMP_FOLDER, diff_directory[0]), os.path.join(TMP_FOLDER, self.package))

        puts(colored.green('Complete!'))

    def pack(self):

        puts(colored.cyan('Generate Minerox Package'))

        if os.path.isfile(os.path.join(SCRIPTS_FOLDER, "tch_{}".format(self.package))):
            sh.cp(os.path.join(SCRIPTS_FOLDER, "tch_{}".format(self.package)), os.path.join(TMP_FOLDER, self.package))
        if os.path.isfile(os.path.join(SCRIPTS_FOLDER, "tch_{}_2".format(self.package))):
            sh.cp(os.path.join(SCRIPTS_FOLDER, "tch_{}_2".format(self.package)), os.path.join(TMP_FOLDER, self.package))

        if os.path.isfile(os.path.join(SCRIPTS_FOLDER, "bsy_{}".format(self.package))):
            sh.cp(os.path.join(SCRIPTS_FOLDER, "bsy_{}".format(self.package)), os.path.join(TMP_FOLDER, self.package))
        if os.path.isfile(os.path.join(SCRIPTS_FOLDER, "bsy_{}_2".format(self.package))):
            sh.cp(os.path.join(SCRIPTS_FOLDER, "bsy_{}_2".format(self.package)), os.path.join(TMP_FOLDER, self.package))

        if self.patch:
            #copy patch as well
            sh.cp(self.patchpath, os.path.join(TMP_FOLDER, self.package))

        tar = tarfile.open("{}/{}.tar.gz".format(VERSION, self.package), "w:gz")
        tar.add(os.path.join(TMP_FOLDER, self.package), arcname=self.package)
        tar.close()
        puts(colored.green('Complete!'))

    def complete(self):
        puts(colored.cyan('Remove Temporary Folders and Files'))

        sh.rm('-rf', os.path.join(TMP_FOLDER, self.filename))
        sh.rm('-rf', os.path.join(TMP_FOLDER, self.package))
        if self.patch:
            # delete the patch file
            sh.rm('-rf', os.path.join(TMP_FOLDER, self.patchpath))

        puts(colored.cyan('Process Complete'))






