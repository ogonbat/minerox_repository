from plugins.base import Plugin


class Kbd(Plugin):
    url = "https://www.kernel.org/pub/linux/utils/kbd/kbd-2.0.4.tar.xz"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.0/kbd-2.0.4-backspace-1.patch"
    package = "kbd"
