from plugins.base import Plugin


class Mpfr(Plugin):
    url = "http://www.mpfr.org/mpfr-3.1.5/mpfr-3.1.5.tar.xz"
    package = "mpfr"
