from plugins.base import Plugin


class Udev(Plugin):
    url = "http://anduin.linuxfromscratch.org/LFS/udev-lfs-20140408.tar.bz2"
    package = "udev"
