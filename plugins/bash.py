from plugins.base import Plugin


class Bash(Plugin):
    url = "http://ftp.gnu.org/gnu/bash/bash-4.4.tar.gz"
    patch = "http://www.linuxfromscratch.org/patches/lfs/8.0/bash-4.4-upstream_fixes-1.patch"
    package = "bash"
