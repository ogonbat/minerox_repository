from plugins.base import Plugin


class Check(Plugin):
    url = "https://github.com/libcheck/check/releases/download/0.11.0/check-0.11.0.tar.gz"
    package = "check"
