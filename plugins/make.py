from plugins.base import Plugin


class Make(Plugin):
    url = "http://ftp.gnu.org/gnu/make/make-4.2.1.tar.bz2"
    package = "make"
