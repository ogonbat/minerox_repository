from plugins.base import Plugin


class Kmod(Plugin):
    url = "https://www.kernel.org/pub/linux/utils/kernel/kmod/kmod-24.tar.xz"
    package = "kmod"
