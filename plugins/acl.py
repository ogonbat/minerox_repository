from plugins.base import Plugin


class Acl(Plugin):
    url = "http://download.savannah.gnu.org/releases/acl/acl-2.2.52.src.tar.gz"
    package = "acl"
