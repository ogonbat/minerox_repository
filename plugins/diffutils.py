from plugins.base import Plugin


class Diffutils(Plugin):
    url = "http://ftp.gnu.org/gnu/diffutils/diffutils-3.6.tar.xz"
    package = "diffutils"
