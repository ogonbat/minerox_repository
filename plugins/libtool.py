from plugins.base import Plugin


class Libtool(Plugin):
    url = "http://ftp.gnu.org/gnu/libtool/libtool-2.4.6.tar.xz"
    package = "libtool"
