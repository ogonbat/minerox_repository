from plugins.base import Plugin


class Autoconf(Plugin):
    url = "http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz"
    package = "autoconf"
