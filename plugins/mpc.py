from plugins.base import Plugin


class Mpc(Plugin):
    url = "http://www.multiprecision.org/mpc/download/mpc-1.0.3.tar.gz"
    package = "mpc"
