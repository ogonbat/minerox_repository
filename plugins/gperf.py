from plugins.base import Plugin


class Gperf(Plugin):
    url = "http://ftp.gnu.org/gnu/gperf/gperf-3.1.tar.gz"
    package = "gperf"
