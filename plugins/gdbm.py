from plugins.base import Plugin


class Gdbm(Plugin):
    url = "http://ftp.gnu.org/gnu/gdbm/gdbm-1.13.tar.gz"
    package = "gdbm"
