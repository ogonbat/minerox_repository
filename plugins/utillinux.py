from plugins.base import Plugin


class Utillinux(Plugin):
    url = "https://www.kernel.org/pub/linux/utils/util-linux/v2.30/util-linux-2.30.1.tar.xz"
    package = "utillinux"
