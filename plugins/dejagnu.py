from plugins.base import Plugin


class Dejagnu(Plugin):
    url = "http://ftp.gnu.org/gnu/dejagnu/dejagnu-1.6.tar.gz"
    package = "dejagnu"
