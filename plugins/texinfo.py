from plugins.base import Plugin


class Texinfo(Plugin):
    url = "http://ftp.gnu.org/gnu/texinfo/texinfo-6.4.tar.xz"
    package = "texinfo"
