from plugins.base import Plugin


class Patch(Plugin):
    url = "http://ftp.gnu.org/gnu/patch/patch-2.7.5.tar.xz"
    package = "patch"
