from plugins.base import Plugin


class Libpipeline(Plugin):
    url = "http://download.savannah.gnu.org/releases/libpipeline/libpipeline-1.4.2.tar.gz"
    package = "libpipeline"
