from plugins.base import Plugin


class Tclcore(Plugin):
    url = "http://sourceforge.net/projects/tcl/files/Tcl/8.6.7/tcl-core8.6.7-src.tar.gz"
    package = "tclcore"
