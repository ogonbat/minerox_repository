from plugins.base import Plugin


class Attr(Plugin):
    url = "http://download.savannah.gnu.org/releases/attr/attr-2.4.47.src.tar.gz"
    package = "attr"
