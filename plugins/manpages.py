from plugins.base import Plugin


class Manpages(Plugin):
    url = "https://www.kernel.org/pub/linux/docs/man-pages/man-pages-4.12.tar.xz"
    package = "manpages"
