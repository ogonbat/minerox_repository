import importlib

OS_LIST = 'os-list'

for soft in open(OS_LIST):
    soft = soft.strip()
    module = importlib.import_module('plugins.{}'.format(soft))
    my_class = getattr(module, soft.capitalize())
    my_instance = my_class()

    if not my_instance.check():
        my_instance.download()
        my_instance.unpack()
        my_instance.pack()
        my_instance.complete()